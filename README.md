#### brought to you by
# MedinaDev.com 
#### Simple Tutorials | Clean Reusable Code Snippets
<br />

# Purpose of this project
This project will show you how to add a Phaser scene to an Angular component along with a simple text label and house image. This is part 1 of a video series on creating/running your very own isometric game on a website using Angular 9 and Phaser 3.

- youtube link: https://youtu.be/FEnoUDeohcE
- for a step by step tutorial, visit:  http://medinadev.com/creating-a-phaser-isometric-tile-based-game-with-angular-episode-1-civ-clone/

# How to run this project?
In root directory, run the following commands:
1. ``npm install ``
1. ``npm start ``
3. navigate to http://localhost:4200/ in your browser

# What is this project doing?
##  ```src\app\app.component.ts```   
- after view initializes, creates a new IsometricScene using ```src\app\isometric-scene.ts``` 
## ```src\app\isometric-scene.ts``` 
- defines a our Phaser scene class
- preloads our house image then creates a house game object in our scene 
-  defines the sceneConfig used by app.component.ts

# What else is included in this project?
### Phaser
   Phaser library was installed to allow your typescript classes to easily use Phaser libraries - defined in package.json
    installed with ```npm i phaser```
### Changes to ``tsconfig.json``
- changed "module" type to "AMD"
- added "scripthost" to "lib"

# About angular-phaser-tiled-isometric Project
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.
<br />
Phaser 3.24.1