export class IsometricScene extends Phaser.Scene {

    public static readonly sceneConfig: Phaser.Types.Core.GameConfig = {
        type: Phaser.AUTO,
        width: 800,
        height: 600,
        parent: 'gameContainer',
        scene: [IsometricScene]
    };

    public preload() {
        console.log('onPreload');
        this.load.image('house', 'assets/house.png');
    }

    public create() {
        console.log('onCreate');
        let text = "Hello from scene!";
        this.add.text(0, 0, text);
        this.add.image(150, 150, 'house')
    }
}
